(() => {
    const elements = document.getElementsByClassName("image-card");
    const downloads = [];
    for (let index = 0; index < elements.length; index++) {
        const element = elements[index];
        const dataKey = element.getAttribute("data-key");
        downloads.push(dataKey);
    }
    browser.runtime.sendMessage({
        downloads: downloads,
    });
})();
