const artbreederUrlRegExp = /https?:\/\/artbreeder\.com\//i

function updateBrowserActionEnabled(url) {
    if (url && typeof (url) === "string" && url.match(artbreederUrlRegExp)) {
        browser.browserAction.enable();
    } else {
        browser.browserAction.disable();
    }
}

browser.tabs.onActivated.addListener(async (activeInfo) => {
    try {
        const tabInfo = await browser.tabs.get(activeInfo.tabId);
        const url = tabInfo.url;
        updateBrowserActionEnabled(url);
    } catch (error) {
        console.error(error);
    }
});

browser.tabs.onUpdated.addListener((tabId, changeInfo, tabInfo) => {
    const url = tabInfo.url;
    updateBrowserActionEnabled(url);
});

browser.browserAction.onClicked.addListener((tab) => {
    browser.tabs.executeScript(tab.id, {
        file: "/page.js",
    });
});

async function downloadUrl(dataKey) {
    const url = `https://s3.amazonaws.com/ganbreederpublic/imgs/${dataKey}.jpeg`;
    const response = await fetch(url);
    const blob = await response.blob();
    browser.downloads.download({
        conflictAction: "overwrite",
        filename: `artbreeder/${dataKey}.jpeg`,
        url: URL.createObjectURL(blob),
    });
}

browser.runtime.onMessage.addListener(async (request, sender, sendResponse) => {
    if (request.downloads) {
        for (let index = 0; index < request.downloads.length; index++) {
            const dataKey = request.downloads[index];
            downloadUrl(dataKey);
        }
    }
});